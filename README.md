# ML5.js : Machine Learning pour le Web

![](https://i.imgur.com/oUXdsKy.png)

*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) - All rights reserved for educational purposes only*

<br>

## Présentation

[ML5.js](https://ml5js.org) vise à rendre l'apprentissage automatique accessible à un large public d'artistes, de codeurs créatifs et d'étudiants. La bibliothèque donne accès aux algorithmes et modèles d'apprentissage automatique dans le navigateur, en s'appuyant sur [TensorFlow.js](https://www.tensorflow.org/js) sans aucune autre dépendance externe.

<br>

## Contenu du répertoire

Le projet présenté dans ce répertoire permet d'utiliser le modèle [MobilNet](https://github.com/tensorflow/tfjs-models/tree/master/mobilenet) pour reconnaitre des images.

Pour simplifier l'utilisation de ce répertoire il vous est fortement conseillé d'installer le module [Live Server](https://www.npmjs.com/package/live-server).